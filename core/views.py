from django.http import HttpResponse


def robots(request):
    return HttpResponse('User-agent: *\nDisallow: /admin/', content_type="text/plain")