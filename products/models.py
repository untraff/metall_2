from django.db import models
from django.utils.text import slugify
from unidecode import unidecode
from django.shortcuts import reverse


class Arma_category(models.Model):
    name = models.CharField(max_length=255)
    seo_text = models.TextField(blank=True)
    image = models.ImageField(upload_to='images/', blank=True)
    slug = models.SlugField(unique=True)
    designation = models.CharField(max_length=100)

    def save(self, *args, **kwargs):
        if not self.pk:
            self.slug = slugify(
                unidecode(str(self.name) + '-' + str(self.designation)))
        super(Arma_category, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.name) + ' ' + str(self.designation)


class Arma_type(models.Model):
    name = models.CharField(max_length=255)
    seo_text = models.TextField(blank=True)
    image = models.ImageField(upload_to='images/', blank=True)
    slug = models.SlugField(unique=True)

    def save(self, *args, **kwargs):
        if not self.pk:
            self.slug = slugify(unidecode(str(self.name)))
        super(Arma_type, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.name)


class Arma_diameter(models.Model):
    name = models.IntegerField()
    seo_text = models.TextField(blank=True)
    image = models.ImageField(upload_to='images/', blank=True)
    slug = models.SlugField(unique=True)
    meters_per_ton = models.IntegerField(blank=True)

    def save(self, *args, **kwargs):
        if not self.pk:
            self.slug = slugify(unidecode(str(self.name)))
        super(Arma_diameter, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.name)

    def get_absolute_url(self):
        return reverse("ProductDetail", kwargs={"slug": self.slug})

    def lol(self):
        return self.products.first().price / self.meters_per_ton


class Arma_steel(models.Model):
    name = models.CharField(max_length=255)
    seo_text = models.TextField(blank=True)
    image = models.ImageField(upload_to='images/', blank=True)
    slug = models.SlugField(unique=True)

    def save(self, *args, **kwargs):
        if not self.pk:
            self.slug = slugify(unidecode(str(self.name)))
        super(Arma_steel, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.name)


class Arma_length(models.Model):
    name = models.DecimalField(max_digits=10, decimal_places=2)
    seo_text = models.TextField(blank=True)
    image = models.ImageField(upload_to='images/', blank=True)
    slug = models.SlugField(unique=True)

    def save(self, *args, **kwargs):
        if not self.pk:
            self.slug = slugify(unidecode(str(self.name)))
        super(Arma_length, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.name)


class Arma_product(models.Model):
    name = models.CharField(max_length=255)
    image = models.ImageField(upload_to='images/', blank=True)
    slug = models.SlugField(unique=True)
    seo_text = models.TextField(blank=True)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    arma_category = models.ForeignKey(
        'Arma_category',
        on_delete=models.CASCADE,
    )
    arma_type = models.ForeignKey(
        'Arma_type',
        on_delete=models.CASCADE,
    )
    arma_diameter = models.ForeignKey(
        'Arma_diameter',
        on_delete=models.CASCADE,
        related_name='products'
    )
    arma_steel = models.ForeignKey(
        'Arma_steel',
        on_delete=models.CASCADE,
    )
    arma_length = models.ForeignKey(
        'Arma_length',
        on_delete=models.CASCADE,
    )

    class Meta:
        ordering = ['price']

    def save(self, *args, **kwargs):
        if not self.pk:
            self.slug = slugify(unidecode(str(self.arma_category) + ' ' + str(self.arma_type) + ' ' + str(
                self.arma_diameter) + ' ' + str(self.arma_steel) + ' ' + str(self.arma_length)))
        super(Arma_product, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.arma_category) + ' ' + str(self.arma_type) + ' ' + str(self.arma_diameter) + ' ' + str(self.arma_steel) + ' ' + str(self.arma_length)

    def get_absolute_url(self):
        return reverse("ArmaDetail", kwargs={"slug": self.arma_diameter.slug, "pk": self.pk})

    def price_per_meter(self):
        return (self.price + 750) / self.arma_diameter.meters_per_ton
