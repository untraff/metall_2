from django import views
from django.conf import settings
from django.core.mail import send_mail
from django.shortcuts import render
from django.utils import timezone
from django.views.generic import DetailView, TemplateView
from django.views.generic.list import ListView

from products.models import (
    Arma_product,
    Arma_diameter,
)


class ProductListView(ListView):

    model = Arma_diameter
    # paginate_by = 100  # if pagination is desired

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        for diameter in self.object_list.all():
            print(diameter.products.all())
        context['title'] = f'Арматура'
        return context


class ProductDetailView(DetailView):

    model = Arma_diameter

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = f'Арматура {self.object.name} мм. в Санкт-Петербурге'
        return context


class ArmaDetailView(DetailView):

    model = Arma_product

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = f' Арматура {self.object.arma_diameter} мм. {self.object.arma_steel} {self.object.arma_length} м.'
        products_gt = list(Arma_product.objects.filter(arma_diameter=self.object.arma_diameter, price__gte=self.object.price)[:2])
        products_lw = list(Arma_product.objects.filter(arma_diameter=self.object.arma_diameter, price__lte=self.object.price)[:2])
        context['similar_products'] = products_gt + products_lw
        print(context['similar_products'])
        return context


class ContactsView(TemplateView):
    template_name = 'static_pages/contacts.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = f'Контакты'
        return context


class DeliveryView(TemplateView):
    template_name = 'static_pages/delivery.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = f'Доставка'
        return context


class OrderView(TemplateView):
    template_name = 'static_pages/order.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = f'Как купить'
        return context


class shestigrannik(TemplateView):
    template_name = 'static_pages/shestigrannik.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = f'Шестигранник'
        return context


class truba(TemplateView):
    template_name = 'static_pages/truba.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = f'Труба'
        return context


class svarnayasetka(TemplateView):
    template_name = 'static_pages/svarnaya-setka.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = f'Сварная сетка'
        return context


class balka(TemplateView):
    template_name = 'static_pages/balka.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = f'Балка'
        return context


class kvadrat(TemplateView):
    template_name = 'static_pages/kvadrat.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = f'Квадрат'
        return context


class krug(TemplateView):
    template_name = 'static_pages/krug.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = f'Круг'
        return context


class listt(TemplateView):
    template_name = 'static_pages/list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = f'Лист'
        return context


class polosa(TemplateView):
    template_name = 'static_pages/polosa.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = f'Полоса'
        return context


class ugol(TemplateView):
    template_name = 'static_pages/ugol.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = f'Угол'
        return context


class shveller(TemplateView):
    template_name = 'static_pages/shveller.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = f'Швеллер'
        return context


class EmailView(views.View):

    context = {}

    def post(self, request):
        name = request.POST.get('your_name', 'Не указано')
        number = request.POST.get('your_number', 'Не указан')
        subject = 'Новая заявка от клиента'
        html_message = f'<h1>Новая заявка от клиента</h1><ul><li><b>имя: </b>{name}</li><li><b>номер телефона:</b> {number}</li></ul>'
        send_mail(subject, 'TEST!!!!', settings.DEFAULT_FROM_EMAIL, [settings.DEFAULT_FROM_EMAIL], html_message=html_message)

        return render(request, 'static_pages/email.html', self.context)
