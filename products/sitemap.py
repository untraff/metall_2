from datetime import datetime

from django.contrib.sitemaps import Sitemap
from products.models import Arma_product, Arma_diameter


class Arma_productSitemap(Sitemap):
    changefreq = "never"
    priority = 0.5

    def items(self):
        return Arma_product.objects.all()

    def lastmod(self, obj):
        return datetime.now()


class ProductDetailSitemap(Sitemap):
    changefreq = "never"
    priority = 0.5

    def items(self):
        return Arma_diameter.objects.all()

    def lastmod(self, obj):
        return datetime.now()


sitemaps = {
    'ProductDetailSitemap': ProductDetailSitemap,
    'Arma_productSitemap': Arma_productSitemap,
}