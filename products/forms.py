from django import forms


class ContactForm(forms.Form):
    your_name = forms.CharField(label='Ваше имя', max_length=100, widget=forms.TextInput(attrs={'class': 'form-control'}))
    your_number = forms.CharField(label='Ваш номер', max_length=100,
                                widget=forms.TextInput(attrs={'class': 'form-control'}))