from .forms import ContactForm


def global_settings(request):
    return {
        'COMPANY_NAME': 'МеталлСервис',
        'PHONE': '<a class="phone" href="tel:88124486816"><nobr>8 (812) 448-68-16</nobr></a>',
        'PHONE_LINK': 'tel:88124486816',
        'EMAIL': '<a class="email" href="mailto:info@metallserves.ru">info@metallserves.ru</a>',
        'ADDRESS_FULL': 'г. Санкт-Петербург, 196240, 6-й Предпортовый проезд, д. 8.',
        'ADDRESS_SHORT': 'СПб, 6-й Предпортовый проезд, д. 8.',
        'FORM': ContactForm(),
    }
