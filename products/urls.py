from django.urls import path
from products.views import ProductListView, ProductDetailView, ContactsView, DeliveryView, \
    shestigrannik, truba, svarnayasetka, balka, kvadrat, krug, polosa, ugol, shveller, listt, OrderView, EmailView, \
    ArmaDetailView

urlpatterns = [
    path('', ProductListView.as_view(), name="ProductList"),
    path('<int:slug>mm/', ProductDetailView.as_view(), name='ProductDetail'),
    path('<int:slug>mm/<int:pk>/', ArmaDetailView.as_view(), name='ArmaDetail'),
    path('contacts/', ContactsView.as_view(), name='Contacts'),
    path('delivery/', DeliveryView.as_view(), name='Delivery'),
    path('order/', OrderView.as_view(), name='Order'),
    path('shestigrannik/', shestigrannik.as_view(), name='shestigrannik'),
    path('truba/', truba.as_view(), name='truba'),
    path('svarnaya-setka/', svarnayasetka.as_view(), name='svarnaya-setka'),
    path('balka/', balka.as_view(), name='balka'),
    path('kvadrat/', kvadrat.as_view(), name='kvadrat'),
    path('krug/', krug.as_view(), name='krug'),
    path('list/', listt.as_view(), name='list'),
    path('polosa/', polosa.as_view(), name='polosa'),
    path('ugol/', ugol.as_view(), name='ugol'),
    path('shveller/', shveller.as_view(), name='shveller'),
    path('email/', EmailView.as_view(), name='email'),
]

